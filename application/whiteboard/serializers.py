from rest_framework import serializers

from whiteboard.models import Stay, PatientPosition, VentilatorChecks, BiologicalReading


class StaySerializer(serializers.ModelSerializer):
    class Meta:
        model = Stay
        fields = ['comment']


class PatientPositionSerializer(serializers.ModelSerializer):
    class Meta:
        model = PatientPosition
        fields = ['patient', 'position', 'position_date', 'position_long', 'position_age']


class VentilatorChecksSerializer(serializers.ModelSerializer):
    class Meta:
        model = VentilatorChecks
        fields = ['ventilator', 'is_done', 'check_time', 'check_age', 'check_age_str', 'name']


class BiologicalReadingSerializer(serializers.ModelSerializer):
    class Meta:
        model = BiologicalReading
        fields = '__all__'
