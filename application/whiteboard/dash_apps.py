import copy
import datetime

import dash_core_components as dcc
import dash_html_components as html
import dash_table
import numpy as np
import pandas as pd
from dash.dependencies import Output, Input, State
from django_plotly_dash import DjangoDash

from whiteboard.constants import REFRESH_INTERVAL, ALERT_COLOR, BIOLOGICAL_READING_COLUMNS, ASSIGNMENT_COLUMNS, \
    BIOLOGICAL_READING_FIGURE, BIOLOGICAL_READING_GRAPH_TYPE, PAO2_FIO2_THRESHOLD
from whiteboard.dao import RoomAssignmentFetcher, BiologicalReadingCacheHandler, delete_biological_readings, \
    update_biological_reading
from whiteboard.models import BiologicalReading, PatientPosition

room_app = DjangoDash('RoomWhiteboard')
room_app.layout = \
    html.Div(
        style={},
        children=[
            dcc.Location(id='url', refresh=False),
            dcc.Interval(
                id='interval-assignment',
                interval=REFRESH_INTERVAL,
                n_intervals=0
            ),
            dash_table.DataTable(
                id='assignment-table',
                columns=ASSIGNMENT_COLUMNS,
                data=[],
                style_as_list_view=True,
                style_cell={
                    'textAlign': 'left',
                    "font-family": "\"Helvetica Neue\", Helvetica, Arial, sans-serif",
                    "padding": "0.5em 1em 0.25em 1em",
                    "font-size": "1vw",
                },
                style_cell_conditional=[
                    {
                        'if': {'column_id': 'check_age'},
                        'textAlign': 'right'
                    },
                    {
                        'if': {'column_id': 'position_age'},
                        'textAlign': 'right'
                    },
                    {
                        'if': {'column_id': 'pao2_fio2'},
                        'textAlign': 'right'
                    },
                    {
                        'if': {'column_id': 'stay_duration'},
                        'textAlign': 'right'
                    }
                ],
                style_header={
                    'backgroundColor': 'white',
                    'fontWeight': 'bold',
                },
                style_table={
                    "min-height": "100%",
                    "height": "100%",
                },
                style_data={
                },
                style_data_conditional=[
                    {
                        'if': {'row_index': 'odd'},
                        'backgroundColor': 'rgb(248, 248, 248)'
                    },
                    {
                        'if': {
                            'column_id': 'pao2_fio2',
                            'filter_query': '{pao2_fio2} <= %s && {pao2_fio2} > 0' % PAO2_FIO2_THRESHOLD
                        },
                        'backgroundColor': ALERT_COLOR,
                        'color': 'white',
                    },
                ],
            ),
        ]
    )


@room_app.expanded_callback(
    Output('assignment-table', 'data'),
    [Input('url', 'pathname'),
     Input('interval-assignment', 'n_intervals')],
)
def callback_assignment(pathname, interval, **kwargs):
    room_id = int(pathname.split('/')[3])  # FIXME ugly
    ra = RoomAssignmentFetcher(room_id)
    return ra.fetch()


stay_table_app = DjangoDash('StayTable')
stay_table_app.layout = \
    html.Div(
        style={},
        children=[
            dcc.Location(id='url', refresh=False),
            dcc.Store(id='edit-memory', data={}),
            dcc.Store(id='adding-rows-memory', data={"rows_added": 0}),
            dcc.Graph(
                id='biological-reading-graph',
                figure=BIOLOGICAL_READING_FIGURE,
            ),
            html.Button(
                'Ajouter un relevé',
                id='editing-rows-button',
                className='btn btn-sm btn-secondary',
                n_clicks=0,
            ),
            dash_table.DataTable(
                id='biological-reading-table',
                column_selectable="multi",
                selected_columns=["pao2_fio2"],
                columns=BIOLOGICAL_READING_COLUMNS,
                data=[],
                style_as_list_view=True,
                style_cell={
                    "font-family": "\"Helvetica Neue\", Helvetica, Arial, sans-serif",
                    "padding": "0.5em 1em 0.25em 1em",
                    "font-size": "0.75vw",
                },
                editable=True,
                row_deletable=True,
            ),
        ]
    )

br_fields_name = [br["id"] for br in BIOLOGICAL_READING_COLUMNS]
br_fields_name_db = []
for br in BIOLOGICAL_READING_COLUMNS:
    if br['id'] != 'pao2_fio2':
        br_fields_name_db.append(br['id'])


def get_stay_id(pathname):
    return int(pathname.split('/')[3])


@stay_table_app.expanded_callback(
    [Output('biological-reading-table', 'data'),
     Output('biological-reading-graph', 'figure'),
     Output('biological-reading-table', 'style_data_conditional'),],
    [Input('url', 'pathname'),
     Input('biological-reading-table', 'selected_columns'),
     Input('adding-rows-memory', 'data'),],
)
def callback_biological_reading(pathname, selected_columns, rows_memory, **kwargs):
    stay_id = get_stay_id(pathname)

    br_list = list(
        BiologicalReading.objects
            .filter(stay__pk=stay_id, is_deleted=False)
            .order_by("-reading_time")
            .values()
    )
    df = pd.DataFrame(br_list)
    if not df.empty:
        df["pao2"] = df["pao2"].astype(float)
        df["fio2"] = df["fio2"].astype(float)
        df["pao2_fio2"] = np.round(df["pao2"] / df["fio2"], 0)
        df = df[br_fields_name]

    style_data = []
    figure = copy.deepcopy(BIOLOGICAL_READING_FIGURE)
    for sc in selected_columns:
        if not df.empty:
            filtered_df = df[~df[sc].isnull()]
            figure["data"].append({
                    'x': filtered_df["reading_time"].to_list(),
                    'y': filtered_df[sc].to_list(),
                    'type': BIOLOGICAL_READING_GRAPH_TYPE.get(sc, 'scatter'),
                    'name': sc,
            })
        style_data.append({'if': {'column_id': sc}, 'background_color': '#D2F3FF'})

    # this shows position should be attached to stay
    pos_list = list(
        PatientPosition.objects
            .filter(patient__stay__pk=stay_id)
            .order_by("-position_date")
    )
    for idx, p in enumerate(pos_list):
        figure['layout']['annotations'].append({
            'x': p.position_date,
            'y': 1,
            'xref': 'x',
            'yref': 'paper',
            'text': p.position_long,
            'showarrow': True,
            'arrowhead': 2,
            'ax': 0,
            'ay': -36,
        })
    return df.to_dict("records"), figure, style_data


@stay_table_app.expanded_callback(
    Output('adding-rows-memory', 'data'),
    [Input('editing-rows-button', 'n_clicks')],
    [State('url', 'pathname'),]
)
def store_new_rows_clicks(n_clicks, pathname, **kwargs):
    if n_clicks > 0:
        stay_id = get_stay_id(pathname)
        now = datetime.datetime.now()
        now = now.replace(microsecond=0)
        new_br = BiologicalReading(stay_id=stay_id, reading_time=now)
        new_br.save()
    return {"rows_added": n_clicks}


@stay_table_app.expanded_callback(
    Output('edit-memory', 'data'),
    [Input('biological-reading-table', 'data_previous')],
    [State('biological-reading-table', 'data'),
     State('url', 'pathname'),]
)
def edit_rows(previous, current, pathname, **kwargs):
    stay_id = get_stay_id(pathname)

    if not previous and not current:
        return {}

    previous_id_helper = {d['id']: d for d in previous}
    current_ids_helper = {d['id']: d for d in current}
    data_deleted = set(previous_id_helper.keys()).difference(current_ids_helper.keys())
    delete_biological_readings(list(data_deleted))

    previous_hash_helper = {BiologicalReadingCacheHandler.hash_data(d): d for d in previous}
    current_hash_helper = {BiologicalReadingCacheHandler.hash_data(d): d for d in current}
    data_modified = set(current_hash_helper.keys()).difference(set(previous_hash_helper.keys()))
    for dm in data_modified:
        row = current_hash_helper[dm]
        update_biological_reading(stay_id, {f: row[f] for f in br_fields_name_db})

    return {}
