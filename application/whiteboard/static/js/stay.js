$(document).ready(function() {
  $('#stay-edit-comment').ajaxForm("#stay-edit-comment-success", "#stay-edit-comment-error", function(){});
  $('#stay-edit-position').ajaxForm("#stay-edit-position-success", "#stay-edit-position-error", update_position);
  $('#stay-edit-ventilator').ajaxForm("#stay-edit-ventilator-success", "#stay-edit-ventilator-error", update_ventilator);
  $('#stay-edit-ventilator-datetimepicker').datetimepicker({
    format: "YYYY-MM-DDTHH:mm"
  });
  $('#stay-edit-position-datetimepicker').datetimepicker({
    format: "YYYY-MM-DDTHH:mm"
  });
});

function update_ventilator(json) {
    //$("#ventilator_last_check").replaceWith(json.check_time)
    $("#ventilator_last_check_age").replaceWith(json.check_age)
    $("#ventilator_name").replaceWith(json.name)
}

function update_position(json) {
    //$("#position_date").replaceWith(json.position_date)
    $("#position_age").html(json.position_age.toFixed(2))
    $("#position_name").html(json.position_long)
    $("#stay-position-image").attr('src', position_images[json.position])
}
