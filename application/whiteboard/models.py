import datetime
import hashlib

from django.db import models


class Patient(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    ipp = models.CharField(max_length=200)
    birth_date = models.DateField()
    weight = models.IntegerField(blank=True, null=True)
    height = models.IntegerField(blank=True, null=True)

    GENDER = (
        ('M', 'Homme'),
        ('F', 'Femme'),
    )
    gender = models.CharField(
        max_length=10,
        choices=GENDER,
        null=True,
        verbose_name='Genre',
    )

    @property
    def age(self):
        if not self.birth_date:
            return None
        today = datetime.date.today()
        return int((today-self.birth_date).days / 365)  # TODO check with Dad if ok

    @property
    def title(self):
        if self.gender == 'M':
            return 'Mr'
        elif self.gender == 'F':
            return 'Mme'
        return 'N/A'

    @property
    def tidal(self):
        if self.gender and self.height:
            if self.gender == 'M':
                x = 50
            else:
                x = 45.5
            vt = round(6 * (x + 0.91 * (self.height - 152.4)))
            return vt
        else:
            return None

    @property
    def current_stay(self):
        opened_stays = self.stay_set.filter(end_date__isnull=True)
        if len(opened_stays) == 1:
            return opened_stays[0]
        elif len(opened_stays) > 1:
            # TODO specifiy error here
            return -1
        return None

    def __str__(self):
        return "%s %s [%s]" % (self.last_name.upper(), self.first_name, self.ipp)

    @property
    def current_position(self):
        return self.patientposition_set.order_by('-position_date')[0]


class PatientPosition(models.Model):
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE)

    POS = (
        ('D', 'Dorsale'),
        ('V', 'Ventrale'),
        ('SLD', 'Semi-Latérale droite'),
        ('SLG', 'Semi-latérale gauche'),
        ('LG', 'Latérale gauche'),
        ('LD', 'Latérale droite'),
    )
    POS_DICT = {
        'D': 'dorsale',
        'V': 'ventrale',
        'SLD': 'semi-Latérale droite',
        'SLG': 'semi-latérale gauche',
        'LG': 'latérale gauche',
        'LD': 'latérale droite',
    }

    position = models.CharField(max_length=200, choices=POS)
    position_date = models.DateTimeField()
    comment = models.TextField(null=True, blank=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    last_updated = models.DateTimeField(auto_now=True)

    @property
    def position_long(self):
        return self.POS_DICT.get(self.position, None)

    @property
    def position_age(self):
        return (datetime.datetime.now() - self.position_date).total_seconds() / 3600

    def __str__(self):
        return "%s in position %s" % (self.patient, self.position)


class Stay(models.Model):
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE)
    iep = models.CharField(max_length=200)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField(null=True, blank=True)
    comment = models.TextField(null=True, blank=True)
    # TODO from, to ?

    @property
    def duration(self): # TODO check with Dad if ok
        if self.end_date:
            return (self.end_date - self.start_date).days
        return (datetime.datetime.utcnow() - self.start_date).days

    @property
    def current_assignment(self):
        return self.assignmentstaff_set.order_by('-start_date').first()

    @property
    def last_biological_reading(self):
        return self.biologicalreading_set.order_by("-reading_time").first()

    def __str__(self):
        if self.end_date:
            return "%s from %s to %s" % (self.patient, self.start_date.date(), self.end_date.date())
        return "%s from %s" % (self.patient, self.start_date.date())


class Room(models.Model):
    name = models.CharField(max_length=200)


class Bed(models.Model):
    room = models.ForeignKey(Room, on_delete=models.PROTECT)
    name = models.CharField(max_length=200)

    def __str__(self):
        return "%s in %s" % (self.name, self.room.name)


class Ventilator(models.Model):
    room = models.ForeignKey(Room, on_delete=models.PROTECT)
    name = models.CharField(max_length=200)

    @property
    def last_check(self):
        checks = list(self.ventilatorchecks_set.order_by('-pk'))
        if checks:
            return checks[0]
        return VentilatorChecks()

    def __str__(self):
        return "%s in %s" % (self.name, self.room.name)


class VentilatorChecks(models.Model):
    ventilator = models.ForeignKey(Ventilator, on_delete=models.PROTECT)
    is_done = models.BooleanField(default=False)
    status = models.CharField(max_length=200, null=True, blank=True)
    comment = models.TextField(null=True, blank=True)
    check_time = models.DateTimeField()
    creation_date = models.DateTimeField(auto_now_add=True)
    last_updated = models.DateTimeField(auto_now=True)

    @property
    def name(self):
        return self.ventilator.name

    @property
    def check_age(self):
        if self.check_time:
            return round((datetime.datetime.now() - self.check_time).total_seconds() / 3600, 1)

    @property
    def check_age_str(self):
        if self.check_time:
            return "%sh" % self.check_age
        return "N/A"


class PatientLocation(models.Model):
    patient = models.OneToOneField(Patient, on_delete=models.CASCADE)
    bed = models.OneToOneField(Bed, on_delete=models.CASCADE)
    ventilator = models.OneToOneField(Ventilator, on_delete=models.CASCADE)

    def __str__(self):
        return "%s in bed %s with ventilator %s" % (self.patient, self.bed.name, self.ventilator.name)


class Staff(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)

    ROLE = (
        ('MED', 'Médecin'),
        ('IDE', 'Infirmière'),
        ('AS', 'Aide-soignante'),
        ('ASH', 'ASH'),
        ('BR', 'Brancardier'),
        ('DV', 'Team DV'),
    )
    role = models.CharField(max_length=200, choices=ROLE)
    phone = models.IntegerField()

    def __str__(self):
        return "%s %s" % (self.first_name, self.last_name.upper())


class Team(models.Model):
    medical_staff = models.ForeignKey(Staff, on_delete=models.CASCADE, related_name='medical_staff')
    paramedical_staff = models.ForeignKey(Staff, on_delete=models.CASCADE, related_name='paramedical_staff')
    as_staff = models.ForeignKey(Staff, on_delete=models.CASCADE, related_name='as_staff')

    def __str__(self):
        return "%s - %s - %s" % (self.medical_staff, self.paramedical_staff, self.as_staff)


class AssignmentStaff(models.Model):
    stay = models.ForeignKey(Stay, on_delete=models.CASCADE)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField(null=True, blank=True)

    @property
    def patient(self):
        return self.stay.patient

    @property
    def bed(self):
        return self.stay.patient.patientlocation.bed

    def ventilator(self):
        return self.stay.patient.patientlocation.ventilator

    def __str__(self):
        return "%s - %s - %s" % (self.stay.patient, self.team, self.start_date)


class BiologicalReading(models.Model):
    stay = models.ForeignKey(Stay, on_delete=models.CASCADE, verbose_name='Séjour')
    reading_time = models.DateTimeField(verbose_name='Date relevé')
    ph = models.FloatField(blank=True, null=True, verbose_name='pH')
    pao2 = models.IntegerField(blank=True, null=True, verbose_name='PaO2')
    pco2 = models.IntegerField(blank=True, null=True, verbose_name='PCO2')
    hco3 = models.FloatField(blank=True, null=True, verbose_name='HCO3')
    sat = models.IntegerField(blank=True, null=True, verbose_name='Saturation')
    hb = models.FloatField(blank=True, null=True, verbose_name='Hb')
    ht = models.FloatField(blank=True, null=True, verbose_name='Ht')
    creat = models.FloatField(blank=True, null=True, verbose_name='Créatininémie')
    clairance = models.FloatField(blank=True, null=True, verbose_name='Clairance')
    k = models.FloatField(blank=True, null=True, verbose_name='K')
    na = models.FloatField(blank=True, null=True, verbose_name='Na')
    peep = models.IntegerField(blank=True, null=True, verbose_name='PEEP')
    fio2 = models.FloatField(blank=True, null=True, verbose_name='FiO2')
    diurese = models.IntegerField(blank=True, null=True, verbose_name='Diurèse')
    is_deleted = models.BooleanField(default=False)

    @property
    def pao2_fio2(self):
        if self.pao2 and self.fio2 and self.fio2 != 0:
            return round(self.pao2 / self.fio2, 2)
        return "N/A"

    def __str__(self):
        return "%s at %s" % (self.stay, self.reading_time)

    def __hash__(self):
        m = hashlib.sha512()
        m.update(str(self.to_dict()).encode("ascii"))
        return m.hexdigest()
