from django.urls import path, include
from rest_framework import routers

from .views import *
from .viewsets import *

router = routers.DefaultRouter()
router.register(r'stays', StayViewSet)
router.register(r'patientpositions', PatientPositionViewSet)
router.register(r'ventilatorchecks', VentilatorChecksViewSet)
router.register(r'biologicalreadings', BiologicalReadingViewSet)

urlpatterns = [
    path('home/', WhiteboardView.as_view(), name='home'),
    path('room/<int:pk>/', RoomView.as_view(), name='room_detail'),
    path('stay/<int:pk>/', StayView.as_view(), name='stay_detail'),
    path('api/', include(router.urls))
]
