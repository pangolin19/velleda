REFRESH_INTERVAL = 10000
ALERT_COLOR = "#dc3545"
ASSIGNMENT_COLUMNS = [
    {"id": "bed", "name": "Lit", "presentation": "markdown"},
    {"id": "patient", "name": "Patient", "presentation": "markdown"},
    {"id": "age", "name": "Âge", "presentation": "markdown"},
    {"id": "stay_start_date", "name": "Entrée", "presentation": "markdown"},
    {"id": "stay_duration", "name": "J", "presentation": "markdown"},
    {"id": "pao2_fio2", "name": "PaO2/FIO2", "presentation": "markdown"},
    {"id": "position", "name": "Position changée..", "presentation": "markdown"},
    {"id": "position_age", "name": "..depuis", "presentation": "markdown"},
    {"id": "ventilator", "name": "Respirateur vérifié..", "presentation": "markdown"},
    {"id": "check_age", "name": "..depuis", "presentation": "markdown"},
    {"id": "medical_staff", "name": "Médecin", "presentation": "markdown"},
    {"id": "paramedical_staff", "name": "IDE", "presentation": "markdown"},
    # {"id": "comment", "name": "Commentaires", "presentation": "markdown"},
]
BIOLOGICAL_READING_COLUMNS = [
    {'id': 'id', 'name': 'Id', 'visible': False, "editable": False},
    {'id': 'reading_time', 'name': 'Date relevé'},
    {'id': 'pao2_fio2', 'name': 'PaO2/FIO2', "selectable": True, "editable": False},
    {'id': 'pao2', 'name': 'PaO2', "selectable": True},
    {'id': 'fio2', 'name': 'FiO2', "selectable": True},
    {'id': 'ph', 'name': 'pH', "selectable": True},
    {'id': 'pco2', 'name': 'PCO2', "selectable": True},
    {'id': 'hco3', 'name': 'HCO3', "selectable": True},
    {'id': 'sat', 'name': 'Saturation', "selectable": True},
    {'id': 'peep', 'name': 'PEEP', "selectable": True},
    {'id': 'hb', 'name': 'Hb', "selectable": True},
    {'id': 'ht', 'name': 'Ht', "selectable": True},
    {'id': 'creat', 'name': 'Créatininémie', "selectable": True},
    {'id': 'clairance', 'name': 'Clairance', "selectable": True},
    {'id': 'diurese', 'name': 'Diurèse', "selectable": True},
    {'id': 'k', 'name': 'K', "selectable": True},
    {'id': 'na', 'name': 'Na', "selectable": True},
]

BIOLOGICAL_READING_GRAPH_TYPE = {
    'pao2': 'bar',
}

BIOLOGICAL_READING_FIGURE = {
    'data': [],
    'layout': {
        'autosize': True,
        'showlegend': True,
        'margin': {
            't': 48,
        },
        'shapes': [],
        'annotations': [],
    }
}

PAO2_FIO2_THRESHOLD = 150
