from django.contrib import admin

from whiteboard.models import *

admin.site.register(Patient)
admin.site.register(PatientPosition)
admin.site.register(PatientLocation)
admin.site.register(Stay)
admin.site.register(Room)
admin.site.register(Bed)
admin.site.register(Ventilator)
admin.site.register(VentilatorChecks)
admin.site.register(Staff)
admin.site.register(Team)
admin.site.register(AssignmentStaff)
admin.site.register(BiologicalReading)
