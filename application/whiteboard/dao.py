import hashlib

from django.core.cache import cache
from django.db import transaction
from django.urls import reverse

from whiteboard.constants import ASSIGNMENT_COLUMNS
from whiteboard.models import Room, Bed, Patient, AssignmentStaff, BiologicalReading


class RoomAssignmentFetcher(object):
    def __init__(self, room_id):
        self.room = Room.objects.get(pk=room_id)

    def fetch(self):
        beds = Bed.objects.filter(room=self.room).order_by("pk")
        patients = Patient.objects.filter(patientlocation__bed__in=beds)
        assignments = AssignmentStaff.objects.filter(end_date__isnull=True, stay__patient__in=patients)

        out = []
        assignments_helper = {a.bed.id: a for a in list(assignments)}
        for b in beds:
            row = {e["id"]: "" for e in ASSIGNMENT_COLUMNS}  # FIXME somewhat ugly
            row["bed"] = b.name
            if b.id in assignments_helper.keys():
                a = assignments_helper.get(b.id)
                row["patient"] = self._generate_patient_link(a)
                row["age"] = a.patient.age
                row["stay_start_date"] = a.stay.start_date.date()
                row["stay_duration"] = a.stay.duration
                row["medical_staff"] = "%s %s" % (
                    a.team.medical_staff.last_name.upper(), a.team.medical_staff.first_name
                )
                row["paramedical_staff"] = "%s %s" % (
                    a.team.paramedical_staff.last_name.upper(), a.team.paramedical_staff.first_name
                )
                row["pao2_fio2"] = a.stay.last_biological_reading.pao2_fio2 if a.stay.last_biological_reading else "N/A"
                row["position"] = a.patient.current_position.position_long
                row["position_age"] = "%sh" % round(a.patient.current_position.position_age, 1)
                row["ventilator"] = "%s" % a.patient.patientlocation.ventilator.name
                row["check_age"] = a.patient.patientlocation.ventilator.last_check.check_age_str
                row["comment"] = a.stay.comment
            out.append(row)
        return out

    @staticmethod
    def _generate_patient_link(assignment):
        link = reverse("stay_detail", kwargs={"pk": assignment.stay.id})
        return "[%s %s %s](%s)" \
               % (assignment.patient.title, assignment.patient.last_name.upper(), assignment.patient.first_name, link)


class BiologicalReadingCacheHandler(object):
    """ Handles cache
    Note that only hash_data method is used currently
    """

    def __init__(self, session_id=None):
        self.session_id = session_id

    def cache(self, data):
        hashed_dict = {self._create_hash_key(d): 0 for d in data}
        cache.set_many(hashed_dict)

    def get_uncached_data(self, data):
        hash_helper = {self._create_hash_key(d): d for d in data}
        cached_keys = set(cache.get_many(list(hash_helper.keys())).keys())
        new_keys = set(hash_helper).difference(cached_keys)
        return [hash_helper[k] for k in new_keys]

    def _create_hash_key(self, d):
        d["reading_time"] = str(d["reading_time"])  # hacky
        return "%s|br_data|%s" % (self.session_id, self.hash_data(d))

    @staticmethod
    def hash_data(d):
        m = hashlib.md5()
        m.update(str(d).encode("ascii"))
        return m.hexdigest()


@transaction.atomic
def delete_biological_readings(id_list):
    res = BiologicalReading.objects.filter(pk__in=id_list).update(is_deleted=True)
    return res


@transaction.atomic
def update_biological_reading(stay_id, br_dict):
    br_dict["stay_id"] = stay_id
    res = BiologicalReading(**br_dict).save()
    return res
