from django.contrib.auth.mixins import LoginRequiredMixin
from django.template.defaulttags import register
from django.views import generic

from whiteboard.models import Room, Stay, PatientPosition, Ventilator


@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)


class WhiteboardView(LoginRequiredMixin, generic.ListView):
    model = Room
    template_name = "whiteboard/home.html"


class RoomView(LoginRequiredMixin, generic.DetailView):
    model = Room
    template_name = "whiteboard/room.html"


class StayView(LoginRequiredMixin, generic.DetailView):
    model = Stay
    template_name = "whiteboard/stay.html"

    POS_IMAGES = {
        'D': 'img/dorsal.png',
        'V': 'img/ventral.png',
        'SLD': 'img/semi_lateral_droit.png',
        'SLG': 'img/semi_lateral_gauche.png',
        'LG': 'img/lateral_gauche.png',
        'LD': 'img/lateral_droit.png',
    }

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["position_choices"] = PatientPosition.POS
        context["ventilator_choices"] = [(v.pk, v.name) for v in Ventilator.objects.all()]
        context["position_images"] = self.POS_IMAGES
        return context
