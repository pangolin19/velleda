from rest_framework import viewsets, permissions

from whiteboard.models import Stay, PatientPosition, VentilatorChecks, BiologicalReading
from whiteboard.serializers import StaySerializer, PatientPositionSerializer, VentilatorChecksSerializer, \
    BiologicalReadingSerializer


class StayViewSet(viewsets.ModelViewSet):
    queryset = Stay.objects.all()
    serializer_class = StaySerializer
    permission_classes = [permissions.IsAuthenticated]


class PatientPositionViewSet(viewsets.ModelViewSet):
    queryset = PatientPosition.objects.all()
    serializer_class = PatientPositionSerializer
    permission_classes = [permissions.IsAuthenticated]


class VentilatorChecksViewSet(viewsets.ModelViewSet):
    queryset = VentilatorChecks.objects.all()
    serializer_class = VentilatorChecksSerializer
    permission_classes = [permissions.IsAuthenticated]


class BiologicalReadingViewSet(viewsets.ModelViewSet):
    queryset = BiologicalReading.objects.all()
    serializer_class = BiologicalReadingSerializer
    permission_classes = [permissions.IsAuthenticated]
