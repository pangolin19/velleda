from django.contrib import admin
from django.urls import path, include, re_path
from django.views.static import serve

from velleda import settings
from whiteboard.dash_apps import *

dash_apps = [room_app, stay_table_app]

urlpatterns = [
    path('whiteboard/', include('whiteboard.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('admin/', admin.site.urls),
    path('django_plotly_dash/', include('django_plotly_dash.urls')),
    path('api-auth/', include('rest_framework.urls'))
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns.append(path('__debug__/', include(debug_toolbar.urls)))
else:
    urlpatterns.append(
        re_path(r'^static/(?P<path>.*)$', serve, {'document_root': settings.STATIC_ROOT,}),
    )
