# Pangolid

Pangolid "velleda" is a web application to help medical staff during the Covid19
pandemic with two main goals :

- Display the current state of the room containing patients : with staff assignation, key KPIs and alerts
- Display all biometrics of a patient : monitor metrics, report position change, ventilator checks and commentaries

This application is written with :

- Django as front and backend
- Dash (from Plotly) as front for tables and graphs
- Nginx as reverse proxy
- Postgres as main database

Following is the main steps to quickly deploy the app.

### How-to

#### Requirements :
- Linux system
- Docker installed (https://www.docker.com/)

#### Installation
For now, some Django settings are supposed to be configured in a file called `local_settings.py` in `application/velleda`
```
cd application/velleda
cp local_settings_sample.py local_settings.py
```
Inside `local_settings.py` there is a `SECRET_KEY` parameter. Fill it with a secret phrase.

Second, build all docker images
```
cd build
docker-compose build
```

Third, create PostGres database using `psql` and command in `build/postgres/init_db.sql`.

Then, execute django-specific initialization commands (still from `build` folder)
```
docker-compose run velleda ./manage.py makemigrations
docker-compose run velleda ./manage.py migrate
docker-compose run velleda ./manage.py createsuperuser
```
The last command will create a superuser with a login password.
*Note that these 3 commands create containers. You can remove them using `docker rm container-id` command.

Finally, you can run everything using
```
docker-compose up -d
```

#### Set Up
In order to use the application, one must start by creating the structure.

- Go to `http://127.0.0.1:8000/admin` and log in using the superuser
- Create rooms, staffs, patients, stays, positions and ventilators
- Go to `http://127.0.0.1:8000/whiteboard/home` and you are good to go !





